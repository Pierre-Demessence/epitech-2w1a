import random

from action import Action
from config import Config

class Mutate():
    def __init__(self):
        self.wrist_rate = Config.getint("Mutate", "WristRate")
        self.elbow_rate = Config.getint("Mutate", "ElbowRate")
        self.shoulder_rate = Config.getint("Mutate", "ShoulderRate")
        self.maximum_rate = Config.getint("Mutate", "MaximumRate")

    def mutate(self, robot_genes, mutation_rate):
        index = 0
        nb_genes = len(robot_genes)
        while index < nb_genes:
            mutate = random.randint(0, self.maximum_rate)
            #For debug purposes
            #print("shoulder rate: " + format(mutate))
            if (mutate < mutation_rate):
                a = Action()
                strengthMin = Config.getint("Action", "StrengthMin")
                strengthMax = Config.getint("Action", "StrengthMax")
                durationMin = Config.getint("Action", "DurationMin")
                durationMax = Config.getint("Action", "DurationMax")
                a.strength = random.randint(strengthMin, strengthMax)
                a.duration = random.randint(durationMin, durationMax)
                robot_genes[index] = a
            index += 1


    def run(self, robots):
        index = 0
        while index < len(robots):
            self.mutate(robots[index].wrist.sequence, self.wrist_rate)
            self.mutate(robots[index].elbow.sequence, self.elbow_rate)
            self.mutate(robots[index].shoulder.sequence, self.shoulder_rate)
            index += 1
