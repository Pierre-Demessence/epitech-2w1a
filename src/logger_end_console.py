from config import Config
from fitness import Fitness

class LoggerEndConsole():
    def __init__(self):
        self.vrep = Fitness()
        self.passShow = False
        pass
    #fed

    def getRobotByID(self, robots, id):
        for r in robots:
            if r.id == id:
                return r
            #fi
        #rof
        return None

    def showRobot(self, robot, n):
        print(" | | - %d) Robot #%d:" % (n, robot.id))
        print(" | | | - Generation : %d" % (robot.generationCreation))
        print(" | | | - Fitness : %f" % (robot.fitness or 0))
        print(" | | | - Distance : %f" % (robot.distance or 0))
        if self.passShow or not Config.getboolean("Logger", "SeeRobot"):
            return
        yes = ["yes", "ye", "y", ""]
        cpass = ["pass", "pas", "pa", "p"]
        choice = input("See it in action ? (Yes/no/pass)\n").lower()
        if choice in yes:
            self.vrep.runRobot(robot)
        elif choice in cpass:
            self.passShow = True
        else:
            pass

    def askShow(self, data):
        no = ["no", "n"]
        choice = ""
        while choice not in no:
            choice = input("See a particular robot (by ID) ? ('no' to pass)\n").lower()
            try:
                choice = int(choice)
                r = self.getRobotByID(data.robots, choice)
                if r:
                    self.vrep.runRobot(r)
                else:
                    print("Robot with ID %d does not exists" % choice)
            except ValueError:
                pass

    def run(self, data):
        try:
            print(" - Results:")
            print(" | - Seed : {}".format(data.seed))
            if data.doDump:
                ddel = "/"
                tdel = ":"
                frmt = "%m{}%d{}%Y %H{}%M{}%S".format(ddel, ddel, tdel, tdel)
                print(" | - Begin : {}".format(data.dateBegin.strftime(frmt)))
                duration = data.dateEnd - data.dateBegin
                hours, remainder = divmod(duration.seconds, 3600)
                minutes, seconds = divmod(remainder, 60)
                print(" | - Duration : %02d:%02d:%02d" % (hours, minutes, seconds))
            #fi
            print(" | - Generations : {}".format(len(data.robotHistory)))
            print(" | - Robot Generated : {}".format(len(data.robots)))
            sortedRobots = sorted(data.robots, key=lambda r: r.fitness or 0, reverse=True)
            nbBest = min(len(sortedRobots), Config.getint("Logger", "NbBest"))
            if nbBest > 0 : print(" | - Best %d Robots :" % nbBest)
            for i in range(0, nbBest):
                self.showRobot(sortedRobots[i], i+1)
            #rof
            if Config.getboolean("Logger", "SeeRobot"):
                self.askShow(data);
        except (KeyboardInterrupt, SystemExit):
            self.vrep.clear()
            raise
        #yrt
    #fed
