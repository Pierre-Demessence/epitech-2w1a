#!/usr/bin/env python3

import sys
import pickle
import signal

from logger import Logger

def signal_handler(signal, frame):
    print("\nExiting.")
    sys.exit(0)
#fed
signal.signal(signal.SIGINT, signal_handler)

def loadFile(filename, logger):
    print(" - Loading {}".format(filename))
    f = open(filename, "r+b")
    history = pickle.load(f)
    for g in history:
        logger.saveGeneration(g, history[g])
    f.close()
    print(" | - Done")
#fed

if len(sys.argv) > 1:
    print("Launched Pickle Loader.")
    l = Logger(None)
    l.doDump = False
    loadFile(sys.argv[1], l)
    l.run()
else:
    print("Should have one parameter.")
