import random

from robot import Robot
from action import Action
from config import Config

class Skynet():

    def run(self, population):
        l = []
        for n in range(population):
            r = Robot()
            r.wrist.sequence = self.generateActions()
            r.elbow.sequence = self.generateActions()
            r.shoulder.sequence = self.generateActions()
            r.generationCreation = 1
            l.append(r)
        return l

    def generateActions(self):
        l = []
        nbAction = Config.getint("Gene", "NbAction")
        nbActionOffset = Config.getint("Gene", "NbActionOffset")
        nb = random.randint(max(0, nbAction - nbActionOffset), nbAction + nbActionOffset)
        for n in range(nb):
            a = Action()
            strengthMin = Config.getint("Action", "StrengthMin")
            strengthMax = Config.getint("Action", "StrengthMax")
            durationMin = Config.getint("Action", "DurationMin")
            durationMax = Config.getint("Action", "DurationMax")
            a.strength = random.randint(strengthMin, strengthMax)
            a.duration = random.randint(durationMin, durationMax)
            l.append(a)
        return l
