from config import Config
import pygal
import sys

class LoggerFitnessGraph():
    def __init__(self):
        pass
    #fed

    def run(self, data):
        self.generationFitnessGraph(data)
        filename = data.folder + "/" + Config.get("Logger", "FitnessPopGraphName")
        title = "Global Fitness - %d Points"
        nbpts = Config.getint("Logger", "FitnessPopGraphNPTS")
        self.populationFitnessGraph(data, filename+"_x1", title % nbpts, nbpts)
        self.populationFitnessGraph(data, filename+"_x2", title % (nbpts * 2), nbpts * 2)
        self.populationFitnessGraph(data, filename+"_x4", title % (nbpts * 4), nbpts * 4)

    def generationFitnessGraph(self, data):
        print(" - Generating Per Generation Fitness Graph...")
        chart = pygal.Bar(truncate_label=42)
        chart.title = "Fitness per Generation"
        chart.x_title = "Generation"
        modulo = (len(data.robotHistory) / 20) if len(data.robotHistory) >= 20 else 1
        chart.x_labels = map(lambda x: str(x) if not x % modulo else '', range(1, len(data.robotHistory) + 1))
        chart.y_title = "Fitness"

        min_fitness = []
        avg_fitness = []
        max_fitness = []
        for g in data.robotHistory:
            tmin = sys.maxsize
            tavg = 0
            tmax = 0
            nb = 0
            for r in data.robotHistory[g]:
                if r.fitness:
                    nb += 1
                    tmin = min(tmin, r.fitness)
                    tavg += r.fitness
                    tmax = max(tmax, r.fitness)
                #fi
            #rof
            tmin = 0 if tmin == sys.maxsize else tmin
            min_fitness.append(tmin)
            tavg = tavg / nb if nb > 0 else 0
            avg_fitness.append(tavg)
            max_fitness.append(tmax)
        #rof
        chart.add("Min Fitness", min_fitness)
        chart.add("Avg Fitness", avg_fitness)
        chart.add("Max Fitness", max_fitness)

        filename = data.folder + "/" + Config.get("Logger", "FitnessGenGraphName")
        chart.render_to_file("{}.svg".format(filename))
        print(" | - Done. File is {}.svg".format(filename))
    #fed

    def populationFitnessGraph(self, data, filename, title, nbpts):
        print(" - Generating Global Fitness Graph with %d Points..." % nbpts)
        chart = pygal.Line(truncate_label=42, show_legend=False, stroke=True, show_dots=False, interpolate='cubic')

        chart.title = title
        chart.x_title = "Individual ID"
        modulo = (len(data.robots) / 20) if len(data.robots) >= 20 else 1
        chart.x_labels = map(lambda x: str(x) if not x % modulo else '', range(1, len(data.robots) + 1))
        chart.y_title = "Fitness"

        fitness = []
        modulo2 = round(len(data.robots) / nbpts) or 1
        for r in data.robots:
            if r.id % modulo2 == 0:
                fitness.append(r.fitness or 0)
            else:
                fitness.append(None)
            #fi
        #rof
        chart.add("Fitness", fitness)
        
        chart.render_to_file("{}.svg".format(filename))
        print(" | - Done. File is {}.svg".format(filename))
