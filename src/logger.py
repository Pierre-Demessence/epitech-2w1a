from config import Config

from logger_end_console import LoggerEndConsole
from logger_age_graph import LoggerAgeGraph
from logger_fitness_graph import LoggerFitnessGraph
from logger_pickle import LoggerPickle

from datetime import datetime
import os

class Logger():
    def __init__(self, seed):
        self.loggers = []
        self.loggers.append(LoggerEndConsole())
        self.loggers.append(LoggerFitnessGraph())
        self.loggers.append(LoggerAgeGraph())
        self.loggers.append(LoggerPickle())
        
        self.seed = seed
        self.robotHistory = {}
        #self.bestRobot = None
        self.robots = []
        
        self.doDump = True
        self.folder = Config.get("Logger", "folder")
        if not os.path.exists(self.folder):
            print(" - Creating %s directory" % self.folder)
            os.makedirs(self.folder)
        self.dateBegin = datetime.now()
        self.dateEnd = datetime.now()
    #fed

    def saveGeneration(self, generation, robots):
        self.robotHistory[generation] = list(robots)
        for r in robots:
            if r not in self.robots:
                self.robots.append(r)
            #if r.fitness and (not self.bestRobot or self.bestRobot.fitness < r.fitness):
                #self.bestRobot = r
            #fi
        #rof
        if Config.getboolean("Logger", "Debug") and self.doDump:
            str = ""
            str += "##### ##### #####\n"
            str += "Generation {}:\n[".format(generation)
            for r in robots:
                str += "Robot {}: {}\n".format(r.id, r)
                str += "]"
            str += "\n"
            print(str)
            self.run()
            print()
        #fi
    #fed

    def run(self):
        self.dateEnd = datetime.now()
        print("Now Logging.")
        for logger in self.loggers:
            logger.run(self)
        #rof
    #fed
