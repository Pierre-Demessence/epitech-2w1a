import random
from dumper import Dumper
from config import Config
from robot import Robot

class SelectionRobot():
    def __init__(self, robot):
        self.robot = robot
        self.fitness_normalized = None
        self.accumulated_fitness = None

class Selection(Dumper):

    def __init__(self):
        self.population_max = Config.getint("Genetic", "Population")
        self.population_to_keep = Config.getint("Genetic", "PopulationToKeep")

    def roulette_wheel(self, robots):
        total_fitness=0
        robots_list = []
        for robot in robots:
            total_fitness += robot.fitness
            robots_list.append(SelectionRobot(robot))
        for robot in robots_list:
            if total_fitness == 0:
                robot.fitness_normalized = 0
            else:
                robot.fitness_normalized = float(robot.robot.fitness) / float(total_fitness)

        population_sorted = sorted(robots_list, key=lambda robot: robot.fitness_normalized, reverse=True)
        accumulated_fitness = 0
        for robot in population_sorted:
            accumulated_fitness += robot.fitness_normalized
            robot.accumulated_fitness = accumulated_fitness

        robots[:] = []
        to_keep = (self.population_max * self.population_to_keep) / 100
        while len(robots) < to_keep:
            random_select = random.random()
            i = 0
            set_in_list = False
            for robot in population_sorted:
                if robot.accumulated_fitness > random_select:
                    robots.append(robot.robot)
                    set_in_list = True
                    del population_sorted[i]
                    break
                i += 1
            #No robot found(means that accumulated fitness failed) so put the first one
            if set_in_list == False:
                robots.append(population_sorted[0].robot)
                del population_sorted[0]

    def run(self, robots):
        self.roulette_wheel(robots)