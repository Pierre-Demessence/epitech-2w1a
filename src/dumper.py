class Dumper:
    def __str__(self):
        sb = []
        for key in sorted(self.__dict__):
            sb.append("{}: '{}'".format(key, self.__dict__[key]))
        return "{"+', '.join(sb)+"}"

    def __repr__(self):
        return self.__str__()
