from robot import Robot
from gene import Gene
from config import Config
import vrep
import time
import threading
import math
import sys
import concurrent.futures

def pprint(str):
    if Config.getboolean("Logger", "Debug"):
        print(str)
    #fi

class Fitness():
    def vrep_init(self):
        vrep.simxFinish(-1)
        self.clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 5000, 5)
        if self.clientID == -1:
            print("Can't find vrep instance")
            sys.exit(0)
        self.opmode = vrep.simx_opmode_oneshot_wait
        vrep.simxStopSimulation(self.clientID, self.opmode)
        ret1, self.wristHandle = vrep.simxGetObjectHandle(self.clientID, "WristMotor", self.opmode)
        ret2, self.elbowHandle = vrep.simxGetObjectHandle(self.clientID, "ElbowMotor", self.opmode)
        ret3, self.shoulderHandle = vrep.simxGetObjectHandle(self.clientID, "ShoulderMotor", self.opmode)
        ret4, self.robotHandle = vrep.simxGetObjectHandle(self.clientID, "2W1A", self.opmode)
        self.stop = False

    def clear(self):
        if self.clientID and self.opmode:
            vrep.simxStopSimulation(self.clientID, self.opmode)
        vrep.simxFinish(-1)
        
    def __init__(self):
        self.clientID = None
        self.timeLimitTest = Config.getint("Fitness", "timeLimitFirstTest") 
        self.UpTimeTest = Config.getfloat("Fitness", "UpTimeTest")
        self.number_of_crossover_points = Config.getint("Crossover", "NbCrossoverPoints")
        self.population_max = Config.getint("Genetic", "Population")
        self.gene_offset = Config.getint("Gene", "NbActionOffset")

    def main_timer(self, duration):
        pprint("beginning timer")
        time.sleep(duration)
        pprint("end timer")
        self.stop = True
        self.wristThread.join()
        self.elbowThread.join()
        self.shoulderThread.join()
        self.stop = False

    def do_action(self, typeAction, robot, numAction):
        while (self.stop == False):
            nextAction = (numAction + 1) % Config.getint("Gene", "NbAction")
            if typeAction == 1:
                action = robot.wrist.nextAction()
                vrep.simxSetJointForce(self.clientID, self.wristHandle, action.strength, self.opmode)
                duration = action.duration
            elif typeAction == 2:
                action = robot.elbow.nextAction()
                vrep.simxSetJointForce(self.clientID, self.elbowHandle, action.strength, self.opmode)
                duration = action.duration
            else:
                action = robot.shoulder.nextAction()
                vrep.simxSetJointForce(self.clientID, self.shoulderHandle, action.strength, self.opmode)
                duration = action.duration

            time.sleep(duration / 1000)
            self.do_action(typeAction, robot, nextAction)

    # def test(self, robot):
    #     pprint("IN")
    #     if (robot.fitness is None):
    #         pprint("STEP1")
    #         # vrep.simxFinish(-1)
    #         clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 5000, 5)
    #         if self.clientID == -1:
    #             pprint("Can't find vrep instance")
    #             sys.exit(0)
    #         pprint("STEP2")
    #         self.opmode = vrep.simx_opmode_oneshot_wait
    #         # vrep.simxStopSimulation(clientID, self.opmode)
    #         ret1, self.wristHandle = vrep.simxGetObjectHandle(clientID, "WristMotor", self.opmode)
    #         ret2, self.elbowHandle = vrep.simxGetObjectHandle(clientID, "ElbowMotor", self.opmode)
    #         ret3, self.shoulderHandle = vrep.simxGetObjectHandle(clientID, "ShoulderMotor", self.opmode)
    #         ret4, self.robotHandle = vrep.simxGetObjectHandle(clientID, "2W1A", self.opmode)
    #         pprint("STEP3")        
    #         # self.vrep_init()
    #         before = vrep.simxGetLastCmdTime(clientID)
    #         vrep.simxStartSimulation(clientID, opmode)
    #         pprint("STEP4")
    #         pret, robotPosBegin = vrep.simxGetObjectPosition(clientID, robotHandle, \
    #                                                          -1, vrep.simx_opmode_streaming)
    #         self.do_action(1, robot, 0)
    #         self.do_action(2, robot, 0)
    #         self.do_action(3, robot, 0)
    #         self.main_timer()
    #         pret, robotPosEnd = vrep.simxGetObjectPosition(clientID, robotHandle, \
    #                                                        -1, vrep.simx_opmode_streaming)
    #         after = vrep.simxGetLastCmdTime(clientID)
    #         robots[i].time = after - before
    #         # distance positive parcourue sur l'axe x
    #         robots[i].distance = robotPosBegin[0] - robotPosEnd[0]
    #         robots[i].fitness = robots[i].distance if robots[i].distance > 0 else 0
    #         pprint("robot num{} : fitness = {}".format(i, robots[i].fitness))
    #         vrep.simxStopSimulation(self.clientID, self.opmode)
    #         vrep.simxFinish(clientID)
    #         pprint("OUT")

    def runRobot(self, robot):
        try:
            self.doRobot(robot)
        except (KeyboardInterrupt, SystemExit):
            self.clear()
            raise

    def run(self, robots, curGen):
        try:
            self.dostuff(robots, curGen)
        except (KeyboardInterrupt, SystemExit):
            self.clear()
            raise

    def doRobot(self, robot):
        duration = Config.getint("Test", "TestDuration") 
        self.vrep_init()
        before = vrep.simxGetLastCmdTime(self.clientID)
        pprint("Simulation started")
        vrep.simxStartSimulation(self.clientID, self.opmode)
        
        self.wristThread = threading.Thread(target=self.do_action, args=(1, robot, 0,))
        self.wristThread.daemon = True;
        self.wristThread.start()
        self.elbowThread = threading.Thread(target=self.do_action, args=(2, robot, 0,))
        self.elbowThread.daemon = True;
        self.elbowThread.start()
        self.shoulderThread = threading.Thread(target=self.do_action, args=(3, robot, 0,))
        self.shoulderThread.daemon = True;
        self.shoulderThread.start()
        self.main_timer(duration)
        
    def dostuff(self, robots, curGen):
        initial_population_size = len(robots)
        i = 0
        duration = self.timeLimitTest + self.timeLimitTest * self.UpTimeTest * (curGen - 1)
        while i < len(robots):
            print(" | | - Testing Robot {} / {}".format(i+1, len(robots)))
            if robots[i].fitness is None:
                self.vrep_init()
                before = vrep.simxGetLastCmdTime(self.clientID)
                pprint("Simulation started")
                vrep.simxStartSimulation(self.clientID, self.opmode)
                pret, robotPosBegin = vrep.simxGetObjectPosition(self.clientID, self.shoulderHandle, \
                                                       -1, vrep.simx_opmode_streaming)

                self.wristThread = threading.Thread(target=self.do_action, args=(1, robots[i], 0,))
                self.wristThread.daemon = True;
                self.wristThread.start()
                self.elbowThread = threading.Thread(target=self.do_action, args=(2, robots[i], 0,))
                self.elbowThread.daemon = True;
                self.elbowThread.start()
                self.shoulderThread = threading.Thread(target=self.do_action, args=(3, robots[i], 0,))
                self.shoulderThread.daemon = True;
                self.shoulderThread.start()
                self.main_timer(duration)

            
                pprint("DO_ACTION ENDED")

                pret, robotPosEnd = vrep.simxGetObjectPosition(self.clientID, self.shoulderHandle, \
                                                       -1, vrep.simx_opmode_streaming)
                after = vrep.simxGetLastCmdTime(self.clientID)
                robots[i].time = after - before

                pprint("endX == {} ; beginX == {}".format(robotPosEnd[0], robotPosBegin[0]))
                # distance positive parcourue sur l'axe x
                robots[i].distance = robotPosBegin[0] - robotPosEnd[0]

                # distance absolue  parcourue sur l'axe x
                # robots[i].distance = robotPosEnd[0] - robotPosBegin[0] \
                #                     if robotPosEnd[0] > robotPosBegin[0]\
                # else robotPosBegin[0] - robotPosEnd[0]

                # distance séparant point de départ et point d'arrivée
                # robots[i].distance = sqrt(sqr(robotPosEnd[0] - robotPosBegin[0]) + \
                                      # sqr(robotPosEnd[1] - robotPosBegin[1]))


                robots[i].fitness = robots[i].distance if robots[i].distance > 0 else 0
                # robots[i].fitness = robots[i].distance - \
                                # (Config.getint("Fitness", "timePonderate") *  robots[i].distance / 100)
                pprint("robot num{} : fitness = {}".format(i, robots[i].fitness))
                vrep.simxStopSimulation(self.clientID, self.opmode)
            i += 1
    pprint("END OF FITNESS")


'''
    def run(self, robots):
        i = 0
        with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
            future_to_robots = {executor.submit(self.test, robot): robot for robot in robots}
            for future in concurrent.futures.as_completed(future_to_robots):
                robot = future_to_robots[future]
        pprint("END OF FITNESS")
'''
