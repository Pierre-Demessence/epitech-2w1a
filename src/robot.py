from gene import Gene
from dumper import Dumper

class Robot(Dumper):
    next_id = 1
    
    def __init__(self):
        self.id = Robot.next_id
        Robot.next_id += 1
        self.wrist = Gene()
        self.elbow = Gene()
        self.shoulder = Gene()
        self.fitness = None
        self.distance = None
        self.time = None
        self.generationCreation = None
