import random
from dumper import Dumper
from config import Config
from robot import Robot
from gene import Gene

class Crossover(Dumper):

    def __init__(self):
        self.number_of_crossover_points = Config.getint("Crossover", "NbCrossoverPoints")
        self.population_max = Config.getint("Genetic", "Population")
        self.gene_offset = Config.getint("Gene", "NbActionOffset")

    def getCrossoverPoints(self, first_parent_gene, second_parent_gene):
        for_first_parent, for_second_parent = [], []
        len1, len2 = len(first_parent_gene), len(second_parent_gene)
        nb = self.number_of_crossover_points

        if nb > len1:
            nb = len1
        if nb > len2:
            nb = len2

        for i in range(nb):
                index1 = random.randrange(1, len1, 1)
                for_first_parent.append(index1)
                if self.gene_offset == 0:
                    index2 = index1
                else:
                    index2 = random.randrange(1, len2, 1)
                for_second_parent.append(index2)

        return sorted(for_first_parent), sorted(for_second_parent)

    @staticmethod
    def setActionInGene(child_gene, parent_gene, start, stop):
        while start < stop:
            child_gene.sequence.append(parent_gene[start])
            start += 1

    @staticmethod
    def getStartAndStop(index, index_array, max_len):
        # If on the first index, start is 0
        if index == 0:
            start = 0
            stop = index_array[index]
        elif index == len(index_array):
            start = index_array[index - 1]
            stop = max_len
        else:
            start = index_array[index - 1]
            stop = index_array[index]
        return start, stop

    def createChildGene(self, child_gene, first_parent_gene, second_parent_gene):
        points_in_first_parent, points_in_second_parent = self.getCrossoverPoints(first_parent_gene, second_parent_gene)
        index = 0
        while index < len(points_in_first_parent) + 1:
            if index % 2 == 0:
                start, stop = Crossover.getStartAndStop(index, points_in_first_parent, len(first_parent_gene))
                parent_gene = first_parent_gene
            else:
                start, stop = Crossover.getStartAndStop(index, points_in_second_parent, len(second_parent_gene))
                parent_gene = second_parent_gene
            self.setActionInGene(child_gene, parent_gene, start, stop)
            index += 1

    # Be careful, this algo will go on an infinite loop if population is 1 or less
    def run(self, robots):
        children = []
        initial_population_size = len(robots)
        while (len(robots) + len(children)) < self.population_max:
            # Get both parents index
            first_parent_index = random.randint(0, initial_population_size - 1)
            second_parent_index = first_parent_index
            while second_parent_index == first_parent_index:
                second_parent_index = random.randint(0, initial_population_size - 1)
            # Initialize child and get parent in list
            child = Robot()
            first_parent = robots[first_parent_index]
            second_parent = robots[second_parent_index]
            # Crossover for wrist
            self.createChildGene(child.wrist, first_parent.wrist.sequence, second_parent.wrist.sequence)
            # Crossover for elbow
            self.createChildGene(child.elbow, first_parent.elbow.sequence, second_parent.elbow.sequence)
            # Crossover for shoulder
            self.createChildGene(child.shoulder, first_parent.shoulder.sequence, second_parent.shoulder.sequence)
            # Append child to population
            children.append(child)
        return children
