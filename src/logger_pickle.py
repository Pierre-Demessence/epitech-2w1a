from datetime import datetime
import pickle

class LoggerPickle():
    def __init__(self):
        pass
    #fed

    def run(self, data):
        if data.doDump:
            ddel = "-"
            tdel = "\uA789" #latin extended ':' for windows compatibility
            frmt = "%m{}%d{}%Y_%H{}%M{}%S".format(ddel, ddel, tdel, tdel)
            filename = data.folder + "/" + "dump_{}.pkl".format(data.dateBegin.strftime(frmt))
            print(" - Generating Dump File")
            f = open(filename, "w+b")
            pickle.dump(data.robotHistory, f)
            f.close()
            print(" | - Done. File is {}".format(filename))
        #fi
    #fed
