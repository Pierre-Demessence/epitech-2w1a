import time
import datetime

class log:
        def __init__(self):
                self.globalListFile = open("../log/array/globalList", "a")
                self.timeBeginLearning = datetime.datetime.fromtimestamp(time.time()).strftime('%d-%m-%H-%M')
                self.thisGenerationsLog = open("../log/array/" + self.timeBeginLearning, "a")
                self.learningEnded = False

        def addGeneration(self, nbGeneration, fitness):
                self.thisGenerationsLog.write(str(nbGeneration) +  " " + str(fitness) + "\n")
                print("generation " + str(nbGeneration) + " logged")

        def endOfLearning(self, fitness):
                if self.learningEnded == False:
                        self.globalListFile.write(self.timeBeginLearning + " " + str(fitness) + "\n")
                        self.learningEnded = True
                        print("Learning logged")
                else:
                        print("Learning already logged")
