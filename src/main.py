#!/usr/bin/env python3

import random
import sys
import signal

from genetic_algorithm import GeneticAlgorithm
from config import Config 

# Get the seed from config file or generate it
if Config.has_option("General", "Seed"):
    try:
        seed = Config.getfloat("General", "Seed")
    except ValueError:
        seed = random.random()
else:
    seed = random.random()
random.seed(seed)

# Magic
ga = GeneticAlgorithm(seed)

def signal_handler(signal, frame):
    print("\nSaving before exiting...")
    ga.logger.saveGeneration(ga.curIt, ga.robots)
    ga.logger.run()
    print("Exiting.")
    sys.exit(0)
#fed
signal.signal(signal.SIGINT, signal_handler)

ga.run(Config.getint("Genetic", "Population"), Config.getint("Genetic", "Generation"))
