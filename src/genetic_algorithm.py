from robot import Robot
from skynet import Skynet
from config import Config
from crossover import Crossover
from selection import Selection
from mutate import Mutate
from logger import Logger
from fitness import Fitness
import random

class GeneticAlgorithm():

    def __init__(self, seed):
        self.curIt = 1
        self.robots = []
        self.skynet = Skynet()
        self.fitnessAlgo = Fitness()
        self.selectionAlgo = Selection()
        self.crossoverAlgo = Crossover()
        self.mutationAlgo = Mutate()
        self.vrepInterface = None
        self.logger = Logger(seed)

    def generateDefaultFitness(self):
        for r in self.robots:
            if r.fitness == None: r.fitness = random.randint(1, 100)

    def autosetGenerationCreation(self):
        for r in self.robots:
            if r.generationCreation == None: r.generationCreation = self.curIt

    def run(self, population, maxIt):
        self.robots = self.skynet.run(population)
        if not Config.getboolean("General", "StepByStep") and not Config.getboolean("Logger", "Debug"):
            print("Genetic Algorithm Running...")
        while(self.curIt <= maxIt):
            print (" - Generation %d / %d" % (self.curIt, maxIt))
            self.autosetGenerationCreation()
            # self.vrepInterface.run(self.robots)
            print (" | - Running VREP and Fitness Algorithm")
            self.fitnessAlgo.run(self.robots, self.curIt)
            self.generateDefaultFitness()
            self.logger.saveGeneration(self.curIt, self.robots)
            if Config.getboolean("General", "StepByStep"):
                input("Press Enter to advance to next Generation")
            print (" | - Running Selection Algorithm")
            self.selectionAlgo.run(self.robots)
            print (" | - Running Crossover Algorithm")
            children = self.crossoverAlgo.run(self.robots)
            print (" | - Running Mutation Algorithm")
            self.mutationAlgo.run(children)
            self.robots.extend(children)
            
            self.curIt += 1
        # After Algorithm
        print(" - Done.")
        self.logger.run()
