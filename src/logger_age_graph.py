from config import Config
import pygal
import sys

class LoggerAgeGraph():
    def __init__(self):
        pass
    #fed

    def run(self, data):
        print(" - Generating Age Graph...")
        chart = pygal.Bar(human_readable=True, show_legend=False, print_values=True)
        chart.title = "Population Age"
        chart.x_title = "Age"
        chart.y_title = "Quantity"

        ids = []
        ages = [0 for x in range(0, len(data.robotHistory))]
        max_age = 0
        min_age = sys.maxsize
        for g in reversed(range(1, len(data.robotHistory)+1)):
            for r in data.robotHistory[g]:
                if r.id not in ids:
                    ids.append(r.id)
                    max_age = max(max_age, (g - r.generationCreation))
                    min_age = min(min_age, (g - r.generationCreation))
                    ages[(g - r.generationCreation)]+=1
                #fi
            #rof
        #rof
        modulo = (max_age / 20) if max_age >= 20 else 1
        ages = ages[min_age:max_age+1]
        chart.x_labels = map(lambda x: str(x) if not x % modulo else '', range(min_age, max_age+1))
        chart.add("Population", ages)

        filename = data.folder + "/" + Config.get("Logger", "AgeGraphName")
        chart.render_to_file("{}.svg".format(filename))
        print(" | - Done. File is {}.svg".format(filename))
    #fed
