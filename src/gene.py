from action import Action
from dumper import Dumper

class Gene(Dumper):
    def __init__(self):
        self.sequence = []
        self.i = 0
        self.loop = 0

    def nextAction(self):
        action = self.sequence[self.i] if len(self.sequence) > 0 else None
        self.i += 1
        if self.i == len(self.sequence):
            self.i = 0
            self.loop += 1
        return action
