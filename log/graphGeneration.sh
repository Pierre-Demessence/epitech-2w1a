dateFirstTest=$(head -c 19 array/globalList)
lastTest=$(tail -n 1 array/globalList)
dateLastTest=${lastTest:0:19}
oldContent=$(sed '8!d' gnuplot_config)
newContent="set xrange ["$dateFirstTest":"$dateLastTest"]"

sed -i "8s/.*/$newContent/" gnuplot_config
gnuplot gnuplot_config
